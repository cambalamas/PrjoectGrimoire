﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MassAttack : MonoBehaviour
{

    [SerializeField] float lifeTime = 5f;

    // Use this for initialization
    void Start()
    {
        //transform.forward = PlayerController.instance.transform.forward;
        transform.rotation = PlayerController.instance.transform.rotation;
        Destroy(gameObject, lifeTime);
    }

    // Update is called once per frame
    void Update() { transform.position += transform.forward; }

}
