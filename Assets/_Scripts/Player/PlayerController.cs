﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    #region Attributes
    [Header("Player Attributes")]
    [SerializeField] private float life;
    [SerializeField] private float totalLife = 100;
    [SerializeField] private float anger;
    [SerializeField] private float totalRage=2;
	[SerializeField] private float rage=0;
    [SerializeField] private float speed; // walking speed
    [SerializeField] private float ySpeed;
    [SerializeField] private float gravity = 9.8f;
    [SerializeField] private float jump = 5; // jump height
    [SerializeField] GameObject ultimateArea;
    [SerializeField] GameObject massAttack;

    [Header("Control Attributes")]
    [SerializeField] private int iDSnake; // know what snake has caught the character
    [SerializeField] public int NumHits { get; set; }
    public bool attack { get; set; }
    [SerializeField] private float dashSpeed = 5f;  // control dash
    [SerializeField] private bool massControl;
    [SerializeField] private float swordDamage;
    [SerializeField] private float ireDamage;
    [SerializeField] private ParticleSystem dashParticles;
	[SerializeField] private GameObject estelaarma;
	[SerializeField] private AudioSource woodSound1;
	[SerializeField] private AudioSource woodSound2;
	[SerializeField] private AudioSource woodSound3;

    [Header("Back end stuff")]
    private bool attack1;
    private bool attack2;
    private float timePass;
    private Vector3 playerMovement;
    private Animator animController;
    private CharacterController charController;
    private static PlayerController playerController;
	[SerializeField] private CapsuleCollider capsuleCollider;
	[SerializeField] private GameObject player;
	[SerializeField] private GameObject hairPlayer;
	private Shader shader1;
	private Shader shader2;

    [Header("Enemies aliases")]
    private const string ENEMY_ZOMBIE = "UC_Zombie"; 
    private const string ENEMY_SNAKE = "UC_Snake";
    private const string ENEMY_STAR = "UC_Octopus";
    private const string ENEMY_BLUE = "UC_Pingu";
    private const string ENEMY_SPIDER = "UC_Spider";
    private const string ENEMY_PIG = "UC_Pig";
    private const string ENEMY_ELEPAHANT = "UC_Elephant";
    private const string ENEMY_NOHEAD = "UC_NoHed";
	private const string ENEMY_FIREBALL = "FireBall(Clone)";


    [Header("Canvas")]
    [SerializeField] Image imageLife;
	[SerializeField] Image imageRage;


    public static PlayerController instance
    {
        get
        {
            if (!playerController)
            {
                playerController = FindObjectOfType(typeof(PlayerController)) as PlayerController;
                if (!playerController)
                    Debug.LogError("There needs to be one active EventManager script on a GameObject in your scene.");
            }
            return playerController;
        }
    }
    #endregion

    #region MonoBehaviour
    void Start()
    {
        totalLife = 100;
		totalRage = 2;
		rage = 0.0f;
        attack = false;
        charController = GetComponent<CharacterController>();
        animController = GetComponent<Animator>();
		shader1 = Shader.Find("Custom/SuperSaiyan");
		shader2 = Shader.Find("Standard");

    }

    void Update()
    {

        var newPos = GetPlayerMovement();

        //////////////////////////////////////////////////
        //////////////// MOVE !
        //////////////////////////////////////////////////

        if (newPos.magnitude > 0f)
        {
            animController.SetBool("run", true);
            var fixSpeed = speed * Time.deltaTime;

            charController.Move(newPos * fixSpeed);
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(newPos * fixSpeed), 0.44f);
            transform.rotation = new Quaternion(0f, transform.rotation.y, 0f, transform.rotation.w);
        }


        //////////////////////////////////////////////////
        //////////////// IDLE !
        //////////////////////////////////////////////////

        else
        {
            animController.SetBool("run", false);
        }


        //////////////////////////////////////////////////
        //////////////// SPOT LIGHT !
        //////////////////////////////////////////////////

        var charLight = GameObject.FindWithTag("CharLight").transform;
        charLight.position = new Vector3(transform.position.x, charLight.position.y, transform.position.z);


        //////////////////////////////////////////////////
        //////////////// ATTACK ULTIMATE !
        //////////////////////////////////////////////////
		if(rage==totalRage){
			player.GetComponent<Renderer> ().material.shader = shader1;
			player.GetComponent<Renderer> ().material.SetFloat("_EmissionAmount", 2.72f);
			hairPlayer.GetComponent<Renderer> ().material.shader = shader1;
			hairPlayer.GetComponent<Renderer> ().material.SetFloat("_EmissionAmount", 2.72f);
		}
        if (Input.GetMouseButtonDown(2))
        {
			Debug.LogWarning ("Rage " + rage + " Total rage " + totalRage);
			if (rage >= totalRage) {
				rage = 0.0f;
				Debug.LogWarning ("Ultimate");
				player.GetComponent<Renderer> ().material.shader = shader2;
				hairPlayer.GetComponent<Renderer> ().material.shader = shader2;
				animController.Play ("Ultimate");
				Instantiate (ultimateArea, transform.position, Quaternion.Euler (-90, 0, 0));
			}
        }


        //////////////////////////////////////////////////
        //////////////// ATTACK CROW CONTROL !
        //////////////////////////////////////////////////

        if (Input.GetMouseButtonDown(1))
        {
            LookToNearestEnemy();
            animController.Play("Dispersion");
            Instantiate(massAttack, transform.position, Quaternion.identity);
        }


        //////////////////////////////////////////////////
        //////////////// ATTACK MELEE !
        //////////////////////////////////////////////////

        if (Input.GetMouseButtonDown(0))
        {
           // LookToNearestEnemy();
            attack = true;
            animController.SetLayerWeight(1, 1f);
            //animController.SetBool("attack", true);
            //StartCoroutine("StopAnimation", "attack");
            if (timePass < 5f)
            {
                if (!attack1)
                {
                    animController.Play("Attack1");
					estelaarma.SetActive (true);
					woodSound1.Play ();
					StartCoroutine ("EstelaArma");
                    AudioManager.instance.Pitch("attack_ha", 0.9f);
                    AudioManager.instance.Play("attack_ha");
                    attack1 = true;
                }
                else if (!attack2)
                {
					estelaarma.SetActive (true);
					StartCoroutine ("EstelaArma");
					woodSound2.Play ();
                    animController.Play("Attack2");
                    AudioManager.instance.Pitch("attack_ha", 0.95f);
                    AudioManager.instance.Play("attack_ha");
                    attack2 = true;
                }
                else // attack3
                {
					estelaarma.SetActive (true);
					StartCoroutine ("EstelaArma");
					woodSound3.Play ();
                    animController.Play("Attack3");
                    AudioManager.instance.Pitch("attack_ha", 0.85f);
                    AudioManager.instance.Play("attack_ha");
                    attack1 = false;
                    attack2 = false;
                    timePass = 0f;
                }
            }
        }


        //////////////////////////////////////////////////
        ////////// ATTCAK TIMMING !
        //////////////////////////////////////////////////

        timePass += 1 * Time.deltaTime;

        if (timePass >= 5f)
		{	
			
            attack1 = false;
            attack2 = false;
            timePass = 0f;
        }

        if (Input.GetMouseButtonUp(0))
        {
            StartCoroutine(ResetAttack());
            StartCoroutine(AttackAnimWeightDown());
        }


        //////////////////////////////////////////////////
        //////////////// JUMP !
        //////////////////////////////////////////////////

        Vector3 posY = new Vector3(0.0f, ySpeed * Time.deltaTime, 0.0f);
        charController.Move(posY);

        if (charController.isGrounded)
        {
            if (Input.GetButtonDown("Jump"))
            {
                ySpeed = jump;
                animController.Play("JumpUp");
            }
        }
        else
        {
            if (Input.GetButtonUp("Jump"))
                animController.Play("JumpDown");
            // This imitate the Mario Bros jump style
            if (!Input.GetButton("Jump"))
                ySpeed -= gravity * 5f * Time.deltaTime;
            else
                ySpeed -= gravity * Time.deltaTime;

        }

        //////////////////////////////////////////////////
        //////////////// DASH !
        //////////////////////////////////////////////////

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            dashParticles.GetComponent<DashEffect>().emit = true;
            StartCoroutine(ResetSpeed(speed));
            speed += dashSpeed;
            animController.Play("Dash");
        }

    }
    #endregion

    #region coroutines

    //
    // Coroutines
    //

    IEnumerator StopAnimation(string animation)
    {
        yield return new WaitForSeconds(0.05f);
        animController.SetBool(animation, false);
    }

    IEnumerator ResetSpeed(float oldSpeed)
    {
        yield return new WaitForSeconds(0.3f);
        speed = oldSpeed;
        dashParticles.GetComponent<DashEffect>().emit = false;
    }

    IEnumerator ResetAttack()
    {
        yield return new WaitForSeconds(0.4f);
        attack = false;
    }

    IEnumerator AttackAnimWeightDown()
    {
        yield return new WaitUntil(() => !attack && !attack1 && !attack2);
        yield return new WaitForSeconds(0.4f);
        animController.SetLayerWeight(1, 0.1f);
    }


	IEnumerator EstelaArma()
	{
		yield return new WaitForSeconds(0.04f);
		estelaarma.SetActive (false);

	}
	IEnumerator ActivateCollider()
	{
		yield return new WaitForSeconds(0.01f);
		capsuleCollider.enabled = true;

	}
    #endregion

    #region helpers

    //
    // Helpers
    //

    // Ignore the Y position from a given Vec3
    Vector3 HumanVec3(Vector3 dir)
    {
        return new Vector3(dir.x, 0f, dir.z).normalized;
    }

    // Compute player movement based on camera axis
    Vector3 GetPlayerMovement()
    {
        Transform cam = Camera.main.transform;

        float V = Input.GetAxis("Vertical");
        float H = Input.GetAxis("Horizontal");

        bool up = V > 0;
        bool right = H > 0;
        bool down = V < 0;
        bool left = H < 0;

        Vector3 newPos = new Vector3(0f, 0f, 0f).normalized;
        if (up) newPos = HumanVec3(cam.forward);
        if (down) newPos = HumanVec3(-cam.forward);
        if (right) newPos = HumanVec3(cam.right);
        if (left) newPos = HumanVec3(-cam.right);
        if (up && right) newPos = HumanVec3(cam.forward + cam.right);
        if (up && left) newPos = HumanVec3(cam.forward + -cam.right);
        if (down && right) newPos = HumanVec3(-cam.forward + cam.right);
        if (down && left) newPos = HumanVec3(-cam.forward + -cam.right);

        return newPos;
    }

    void LookToNearestEnemy()
    {
        var enemies = GameObject.FindGameObjectsWithTag("Enemy");
        if (enemies.Length > 0)
        {
            var v = Vector3.zero;
            var d = Mathf.Infinity;
            foreach (var enemy in enemies)
            {
                var dist = (transform.position - enemy.transform.position).magnitude;
                if (dist < d)
                {
                    d = dist;
                    v = enemy.transform.position;
                }
            }

            v.y = transform.position.y;
            transform.LookAt(v);
            CameraController.instance.LookToPoint(v);
        }
    }
    #endregion


    #region Trigger
    private void OnTriggerEnter(Collider other)
    {
        switch (other.name)
        {
            case (ENEMY_ZOMBIE):
                Debug.LogWarning("ZOMBIE ATAQUE!!!!!!!!!!!!!!!!!!!!!!!!");
                if (other.gameObject.GetComponent<Enemy>().isAttacking == true)
                { // enemyattack
                    life += other.gameObject.GetComponent<Enemy>().hurt;
				RestLife(other.gameObject.GetComponent<Enemy>().hurt/100);
					rage = rage + 0.25f;
					SumRage (0.025f);
				capsuleCollider.enabled = false;
				StartCoroutine ("ActivateCollider");
                }
                break;

		case (ENEMY_SPIDER):
			Debug.LogWarning("SPIDER ATAQUE!!!!!!!!!!!!!!!!!!!!!!!!");
			if (other.gameObject.GetComponent<Enemy>().isAttacking == true)
			{ // enemyattack
				life += other.gameObject.GetComponent<Enemy>().hurt;
				RestLife(other.gameObject.GetComponent<Enemy>().hurt/100);
				rage = rage + 0.25f;
				SumRage (0.025f);
				capsuleCollider.enabled = false;
				StartCoroutine ("ActivateCollider");
			}
			break;

		case (ENEMY_NOHEAD):
			Debug.LogWarning("NO_HEAD ATAQUE!!!!!!!!!!!!!!!!!!!!!!!!");
			if (other.gameObject.GetComponent<Enemy>().isAttacking == true)
			{ // enemyattack
				life += other.gameObject.GetComponent<Enemy>().hurt;
				RestLife(other.gameObject.GetComponent<Enemy>().hurt/100);
				rage = rage + 0.25f;
				SumRage (0.025f);
				capsuleCollider.enabled = false;
				StartCoroutine ("ActivateCollider");
			}
			break;

		case (ENEMY_SNAKE):
			Debug.LogWarning("NO_HEAD ATAQUE!!!!!!!!!!!!!!!!!!!!!!!!");
			if (other.gameObject.GetComponent<Enemy>().isAttacking == true)
			{ // enemyattack
				life += other.gameObject.GetComponent<Enemy>().hurt;
				RestLife(other.gameObject.GetComponent<Enemy>().hurt/100);
				rage = rage + 0.25f;
				SumRage (0.025f);
				capsuleCollider.enabled = false;
				StartCoroutine ("ActivateCollider");
			}
			break;


		case (ENEMY_FIREBALL):
			Debug.LogWarning ("FIREBALL ATAQUE!!!!!!!!!!!!!!!!!!!!!!!!");
			switch (other.tag) {
				case (ENEMY_BLUE):
					
					life += GameObject.Find ("UC_Pingu").GetComponent<Enemy> ().hurt;
					RestLife (GameObject.Find ("UC_Pingu").GetComponent<Enemy> ().hurt/100);
					rage = rage + 0.25f;
					SumRage (0.025f);
					capsuleCollider.enabled = false;
					StartCoroutine ("ActivateCollider");
					break;

			case (ENEMY_ELEPAHANT):

				life += GameObject.Find ("UC_Elephant").GetComponent<Enemy> ().hurt;
				RestLife (0.3f);
					rage = rage + 0.25f;
					SumRage (0.025f);
					capsuleCollider.enabled = false;
					StartCoroutine ("ActivateCollider");
					break;


			case (ENEMY_STAR):

				life += GameObject.Find ("UC_Octopus").GetComponent<Enemy> ().hurt;
				RestLife (GameObject.Find ("UC_Octopus").GetComponent<Enemy> ().hurt/100);
				rage = rage + 0.25f;
				SumRage (0.025f);
				capsuleCollider.enabled = false;
				StartCoroutine ("ActivateCollider");
				break;

			

			}



			break;


        }
    }
    #endregion

    #region stats manager
    void RestLife(float life)
    {
        Go.to(imageLife, 0.25f, new GoTweenConfig().floatProp("fillAmount", (imageLife.fillAmount - life)));
    }

	void SumRage(float rage)
	{
		Go.to(imageRage, 0.25f, new GoTweenConfig().floatProp("fillAmount", (imageRage.fillAmount + rage)));
	}
    #endregion
}
