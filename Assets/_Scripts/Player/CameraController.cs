﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    #region Attributes

    [Header("Camera dependencies")]
    [SerializeField]
    private Transform target;

    [Header("Camera settings")]
    [SerializeField]
    private float RotX;
    [SerializeField]
    private float RotY;
    [SerializeField]
    private bool verticalInvert;
    [SerializeField]
    private Vector2 vLim = new Vector2(0f, 0f);

    #endregion


    public static CameraController instance;

    private bool isAPointToLook = false;
    private bool loadNextFrame = false;
    private Vector3 pointToLook;
    bool fixTarget = true;

    void Awake()
    {

        // Singleton
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }

        verticalInvert = (PlayerPrefs.GetInt("Y-Axis") == 1) ? true : false;
        RotY = target.transform.eulerAngles.y;
    }

    void Update()
    {
        // Move attached to player
        transform.position = target.position;

        if (Input.GetKey(KeyCode.C))
        {
            fixTarget = !fixTarget;
        }

        // Not enemy or object to re-target
        if (!isAPointToLook)
        {
            // Settings
            float inv = (verticalInvert) ? -1f : 1f;
            // Input
            float inRotY = Input.GetAxisRaw("Mouse X") * 2f;
            float inRotX = Input.GetAxisRaw("Mouse Y");
            // Output
            float outRotY = transform.localEulerAngles.y + inRotY;
            float outRotX = transform.localEulerAngles.x + inRotX;
            //outRotX = Mathf.Clamp(outRotX, -50f, vLim.y);

            //if (outRotX < -15f)
            //{
            //    outRotX = 0f;
            //    var TLA = transform.localEulerAngles;
            //    transform.localEulerAngles = new Vector3(-15f, TLA.y, TLA.z);
            //}

            // Apply rotation
            transform.rotation = Quaternion.Euler(0f, outRotY, 0f);
        }

        // Target an enemy or object
        else
        {
            var R = transform.rotation;
            transform.LookAt(pointToLook);
            var Rp = transform.rotation;
            transform.rotation = R;
            var QL = Quaternion.Lerp(R, Rp, 0.1f);
            transform.rotation = QL;
            if (CompareQuaternions(R, Rp, 0.1f) && !fixTarget) { isAPointToLook = false; }
        }
    }

    public void LookToPoint(Vector3 point)
    {
        pointToLook = point;
        isAPointToLook = true;
    }

    bool CompareQuaternions(Quaternion q1, Quaternion q2, float margin)
    {
        return Mathf.Abs(Quaternion.Dot(q1, q2)) > 1f - margin;
    }


}
