﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ultimate : MonoBehaviour {

	public float maxScale = 1000.0f;
	private Vector3 firstScale;
	// Use this for initialization
	void Start () {
		firstScale = transform.localScale;
		transform.localScale = new Vector3(0.1f,0.1f,0.1f);
		//transform.Rotate(new Vector3(90f, 0f, 0f), Space.Self);
	}
	
	// Update is called once per frame
	void Update () {
		
		transform.localScale += new Vector3(20f,20f,15f);
		if (transform.localScale.x >= maxScale) {
			Destroy(this.gameObject);
		}
	}
}
