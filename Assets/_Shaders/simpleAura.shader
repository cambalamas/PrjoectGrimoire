﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/simpleAura" {
	Properties {
		_AuraColor("Aura Color", Color) = (1,1,0,1)
		_EdgeColor("Edge Color", Color) = (1,1,0,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_NoiseTex("Noise Texture", 2D) = "white" { }
		_Offset ("Aura distance", Float) = 0.1
		_Scale ("Noise Scale", Range(0,0.2)) = 0.01
		_SpeedX ("Noise X speed", Float) = 1.0
		_SpeedY ("Noise Y speed", Float) = 1.0
		_RimPower("Rim Power", Range(0.01, 10.0)) = 1
		_Brightness("Brightness", Range(0.5, 3)) = 2
		_Opacity ("Opacity", Range(0,10)) = 1.0
		_Edge("2nd Rim Size", Range(0,10)) = 0.1
	}
	SubShader {
		Tags { "Queue"="Overlay" "RenderType"="Transparent"  }
		LOD 200

		Pass{

			Name "Aura"
			Cull Back
			ZWrite Off
			ColorMask RGB

			Blend SrcAlpha OneMinusSrcAlpha
			CGPROGRAM
			#include "UnityCG.cginc"		
			//#pragma surface surf Standard fullforwardshadows alpha:blend
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0

			sampler2D _MainTex;
			sampler2D _NoiseTex;		

			struct Input {
				float4 pos;
				float2 uv_MainTex;
				float3 viewDir;
				float3 localPos;
			};
			struct appdata {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float2 uvText : TEXCOORD0;
			};
			struct v2f {
				float4 pos : SV_POSITION;
				float2 uv_Tex : TEXCOORD0;
				float3 viewDir : TEXCOORD1;
				float3 normalDir : TEXCOORD2;
			};

			fixed4 _AuraColor;
			fixed4 _EdgeColor;
			float _Offset;
			float _Scale;
			float _SpeedX, _SpeedY, _RimPower, _Opacity, _Edge, _Brightness;

			v2f vert(appdata iov) {
				
				//UNITY_INITIALIZE_OUTPUT(Input,i);

				v2f o;
				o.pos = UnityObjectToClipPos(iov.vertex);
				//v = v / v.w;
				float3 norm  = normalize(mul ((float3x3)UNITY_MATRIX_IT_MV, iov.normal));
				float2 offset = TransformViewToProjection(norm.xy);
				o.pos.xy += offset * _Offset;

				o.normalDir = normalize(mul(float4(iov.normal, 0), unity_WorldToObject).xyz);
				o.viewDir = normalize(WorldSpaceViewDir(iov.vertex));
				o.uv_Tex = iov.uvText;

				return o;
			}

			fixed4 frag(v2f i) : SV_Target {

				//return _AuraColor;

				//float2 uvNoise;
				//uvNoise.x = i.uv_Tex.x - (_Time.x*_SpeedX);
				//uvNoise.y = i.uv_Tex.y - (_Time.y*_SpeedY);
				float2 uvNoise = float2(i.pos.x* _Scale - (_Time.x * _SpeedX), i.pos.y * _Scale - (_Time.x * _SpeedY));
				fixed4 noise = tex2D (_NoiseTex, uvNoise);
				float viewAngle = saturate(dot(i.viewDir, i.normalDir));
				float4 rim = pow(viewAngle, _RimPower) - noise;	// saturate(x) = clamp(x,0,1)
				float4 tRim = saturate(rim.a*_Opacity);
				float4 extraRim = (saturate((_Edge + rim.a)*_Opacity) - tRim) * _Brightness;
				float outline = 1.0;
				if (viewAngle < 0.31) {
					outline = 0.0;
				}
				fixed4 result = _AuraColor*tRim + _EdgeColor*extraRim;
				result.a *= viewAngle + 0.25;
				return result;
			}
			ENDCG
		} // End of 1st pass
				
		// 2nd PASS
		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutputStandard o) {

			fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
			o.Albedo = c.rgb;
		}
		ENDCG
		// End of 2nd Pass
	}
	FallBack "Diffuse"
}
